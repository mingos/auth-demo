const crypto = require("crypto");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const InvalidCredentialsError = require("../errors/InvalidCredentialsError").InvalidCredentialsError;
const AuthError = require("../errors/AuthError").AuthError;

class AuthService {
	constructor() {
		const hash = crypto.createHash("sha256");
		hash.update(new Date().toString());
		this.jwtKey = hash.digest("base64");
	}

	/**
	 * @param {String} password
	 * @returns {Promise.<String>}
	 */
	hashPassword(password) {
		return new Promise((resolve, reject) => {
			bcrypt.hash(password, 10, (err, hash) => {
				if (err) {
					reject(err);
				}
				resolve(hash);
			});
		});
	}

	/**
	 * @param {String} password
	 * @param {String} hashedPassword
	 * @returns {Promise}
	 */
	verifyPassword(password, hashedPassword) {
		return new Promise((resolve, reject) => {
			bcrypt.compare(password, hashedPassword, (err, areSame) => {
				if (err) {
					reject(err);
				}
				if (areSame) {
					resolve();
				} else {
					reject(new InvalidCredentialsError());
				}
			});
		});
	}

	/**
	 * @param {String} userId
	 * @returns {String}
	 * @private
	 */
	_generateRefreshToken(userId) {
		const hash = crypto.createHash("sha256");
		hash.update(userId + new Date().toString());
		return hash.digest("base64");
	}

	/**
	 * @param {String} userId
	 * @param {String} refreshToken
	 * @returns {String}
	 * @private
	 */
	_generateAccessToken(userId, refreshToken) {
		const jwtPayload = {data: userId, refresh: refreshToken};
		return jwt.sign(jwtPayload, this.jwtKey, {expiresIn: "5m"});
	}

	/**
	 * @param {User} user
	 * @returns {*}
	 */
	generateAuthResponse(user) {
		const refreshToken = this._generateRefreshToken(user._id);
		const accessToken = this._generateAccessToken(user._id, refreshToken);

		return {
			user,
			access_token: accessToken,
			refresh_token: refreshToken
		};
	}

	/**
	 * @param {String} header
	 * @returns {Promise.<String>}
	 */
	extractToken(header) {
		return Promise.resolve(header)
			.then(header => {
				if (!header) {
					throw new AuthError("Missing auth header.");
				}

				return header;
			})
			.then(header => {
				const token = header.replace("Bearer ", "");
				if (!token) {
					throw new AuthError("Missing auth token in header.");
				}

				return token;
			});
	}

	/**
	 * @param {String} token
	 * @returns {Promise}
	 */
	verifyToken(token) {
		return Promise.resolve(token)
			.then(token => {
				let result = false;
				try {
					result = jwt.verify(token, this.jwtKey);
				} catch (e) {
					throw new Error(`Invalid auth token: ${e.message}.`);
				}

				if (!result) {
					throw new Error("Invalid auth token.");
				}

				return token;
			});
	}

	/**
	 * @param {String} token
	 * @returns {*}
	 */
	decodeToken(token) {
		return jwt.decode(token);
	}
}

exports.authService = new AuthService();
