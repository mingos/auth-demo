const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

mongoose.Promise = Promise;
mongoose.connect("mongodb://127.0.0.1:27017/auth");

const port = process.env.PORT || 3000;
const app = express();

app.use(bodyParser.json());

app.use("/auth", require("./routes/signup").signup);
app.use("/auth", require("./routes/login").login);
app.use("/auth", require("./routes/refresh").refresh);
app.use("/users", require("./routes/users").users);

app.use((err, req, res, next) => {
	if (!(err instanceof Error)) {
		err = new Error(err);
	}
	err.status = err.status || 500;

	res.status(err.status).send({
		message: err.message,
		status: err.status,
		stack: err.stack
	});
});

app.listen(port, () => {
	console.log(`Magic happens on port ${port}`);
});
