class AuthError extends Error {
	constructor(message) {
		super(message || "Invalid auth token.");
		this.status = 401;
	}
}

exports.AuthError = AuthError;
