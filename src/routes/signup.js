const express = require("express");
const User = require("../models/user").User;
const authService = require("../services/authService").authService;

const signup = express.Router();

signup.post("/signup", (req, res, next) => {
	const user = new User({
		username: req.body.username
	});

	authService.hashPassword(req.body.password)
		.then(hash => user.password = hash)
		.then(() => user.save())
		.then(() => res.status(201).send(user))
		.catch(err => next(err));
});

exports.signup = signup;
