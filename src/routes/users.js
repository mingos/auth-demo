const express = require("express");
const User = require("../models/user").User;
const isAuthenticated = require("../middlewares/isAuthenticated").isAuthenticated;

const users = express.Router();

users.get("/", isAuthenticated, (req, res, next) => {
	return User.find({}).exec()
		.then(result => res.send(result))
		.catch(err => next(err));
});

exports.users = users;
