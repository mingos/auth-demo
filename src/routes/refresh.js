const express = require("express");
const User = require("../models/user").User;
const AuthError = require("../errors/AuthError").AuthError;
const authService = require("../services/authService").authService;


const refresh = express.Router();

refresh.post("/refresh", (req, res, next) => {
	authService.extractToken(req.header("Authorization"))
		.then(token => authService.decodeToken(token))
		.then(decoded => {
			if (decoded.refresh !== req.body.refresh) {
				throw new AuthError("Invalid refresh token.");
			}

			return decoded;
		})
		.then(decoded => User.findById(decoded.data).exec())
		.then(user => {
			if (!user) {
				throw new AuthError("Invalid auth token.");
			}

			res.send(authService.generateAuthResponse(user));
		})
		.catch(err => next(err));
});

exports.refresh = refresh;
