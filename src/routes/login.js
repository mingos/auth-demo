const express = require("express");
const User = require("../models/user").User;
const InvalidCredentialsError = require("../errors/InvalidCredentialsError").InvalidCredentialsError;
const authService = require("../services/authService").authService;

const login = express.Router();

login.post("/login", (req, res, next) => {
	let user;

	User.findOne({username: req.body.username}).exec()
		.then(result => {
			if (!(user = result)) {
				throw new InvalidCredentialsError();
			}
		})
		.then(() => authService.verifyPassword(req.body.password, user.password))
		.then(() => res.send(authService.generateAuthResponse(user)))
		.catch(err => next(err));
});

exports.login = login;
