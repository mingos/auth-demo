const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
	username: {type: String, unique: true},
	password: String
}, {
	collection: "users"
});

exports.User = mongoose.model("User", userSchema);
