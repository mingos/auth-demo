const User = require("../models/user").User;
const AuthError = require("../errors/AuthError").AuthError;
const authService = require("../services/authService").authService;

exports.isAuthenticated = (req, res, next) => {
	authService.extractToken(req.header("Authorization"))
		.then(token => authService.verifyToken(token))
		.then(token => authService.decodeToken(token))
		.then(decoded => User.findById(decoded.data).exec())
		.then(user => {
			if (!user) {
				throw new AuthError();
			}
			next();
		})
		.catch(err => next(err));
};
