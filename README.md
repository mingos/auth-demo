# Auth demo

This project is intended to show how token-based authentication can be added to a Node.js application without any external authentication libraries.

The code presented is not production ready and is written with simplicity and readability in mind. The authentication mechanism is not intended to be compliant with OAuth or any other standard.

The changes in the code are described in steps, each step corresponding to a commit in the repository's history. Feel free to look at the diffs of every commit to see what changes were introduced.

## Installation and running

Assuming MongoDB is available on default host and port (127.0.0.1:27017):

```
npm install
npm start
```

If MongoDB is not available, this project provides Docker commands to download and run an instance of MongoDB:

```
npm install
npm run docker:pull
npm run docker:start
npm start
```

The `docker:pull` command needs to be run only once. The `docker:start` needs to be run whenever the docker container should be started to expose MongoDB on the default host & port. The container can be stopped as well:

```
npm run docker:stop
```

## Steps

### 1. Empty project

We start with an "empty" project containing only a simple server (using express.js, but any other framework could be used here) and a single route that will be used later on to fetch a list of users. For now, it returns a hard-coded value.

### 2. Create user model

We need to add a dependency in order to use MongoDB. We use mongoose, an easy to use ODM.

Next, we create a `User` model. It only needs to store the username and password. An email field could be used in place of the username.

Finally, since we can now select users from the database, we replace the hard-coded value in the GET /users endpoint with a dynamically fetched collection.

### 3. Create POST /auth/signup endpoint

We add a new POST /auth/signup endpoint, wherein we receive the new user data and store it in the database. For now, the password is stored unencrypted.

### 4. Create POST /auth/login endpoint

We add a new POST /auth/login endpoint, accepting a username and password in the request body and responding with the matchig user entity along with a short-lived JWT (JSON Web Token). For now, the JWT is not yet used anywhere and its encryption key is a hard-coded value.

We also create a custom error, `InvalidCredentialsError`. It is thrown whenever the login request data contains an invalid username or password. The custom error has its status set up, so the error handler will know what response status should be set.

### 5. Hash passwords

In order to improve security, the passwords need to be hashed. We use the bcrypt module for this.

The signup endpoint now hashes the received password before storing the user entity in the database.

The login endpoint, instead of comparing the received password with the stored clear text value, uses bcrypt comparison to see if the received password hash is the same as the stored  one.

### 6. Authentication for GET /users endpoint

We will now make the GET /users endpoint require authentication. The `Authorization` header will be read and if it contains a valid token, it will be used to authenticate the user.

We start by Getting the header contents in the users controller. The token is extracted, verified and decoded. Each of these steps can throw an error: an `AuthError`, created specifically for this purpose.

Only when the user ID found in the decoded JWT is valid, will the controller continue to return the users list.

The error handling is changed because the authentication throws errors instead of strings. Instead of making sure the `.catch` methods pass `Error`s to the error handler, we let the error handler decide what to do with the received data.

### 7. Auth middleware

Since there can be more routes that will require authentication, it would be silly to repeat the authentication code over and over. We will therefore move the code to a dedicated middleware.

The only change in the middleware code is the addition of a `next()` call, which allows the controller code to be execueted.

### 8. Refresh token

Since auth tokens are short-lived, the user needs a way to refresh the token in order to continue using the API. This is what refresh tokens are for.

Using the crypto package, we add a randomly generated refresh token and add it to the auth token as well as the login response.

We also add a POST /auth/refresh endpoint. It accepts the expired auth token and also requires that the user send the refresh token in the request body. If the token matches the value in the decoded JWT, a new access and refresh token pair is generated.

### 9. Auth service

Single responsibility principle mandates that auth code be part of a specialised service. It should not be used, often repeatedly, across controllers.

The service exposes the following methods:

* `hashPassword(password)` - creates a password hash
* `verifyPassword(password, hashedPassword)` - checks whether an open text password matches the hashed one
* `generateAuthResponse(user)` - creates an object that will be sent in the API response, containing the user object, access token and refresh token
* `extractToken(header)` - extracts the encoded access token from the header value
* `verifyToken(token)` - checks whether the encoded token is valid (not malformed or expired)
* `decodeToken(token)` - decodes a token, returning its contents

Also, JWT secret is generated randomly.
